package br.edu.uniateneu.webservicesubjetivanp1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebserviceSubjetivaNp1Application {

	public static void main(String[] args) {
		SpringApplication.run(WebserviceSubjetivaNp1Application.class, args);
	}

}
